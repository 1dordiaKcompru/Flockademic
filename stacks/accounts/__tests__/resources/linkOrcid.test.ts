jest.mock('../../src/dao', () => ({
  getAccountForOrcid: jest.fn().mockReturnValue(Promise.resolve(null)),
  getSessionForToken: jest.fn().mockReturnValue(Promise.resolve({ id: 'arbitrary_user_id' })),
  storeOrcidCredentials: jest.fn().mockReturnValue(Promise.resolve()),
}));
jest.mock('../../../../lib/lambda/signJwt', () => ({
  signJwt: jest.fn().mockReturnValue('Arbitrary JWT'),
}));
jest.mock('../../../../lib/lambda/verifyJwt', () => ({
  verifyJwt: jest.fn().mockReturnValue({ identifier: 'Arbitrary account ID' }),
}));
jest.mock(
  'node-fetch',
  () => ({
    default: jest.fn().mockReturnValue(Promise.resolve({ json: () => Promise.resolve({ orcid: 'arbitrary orcid' }) })),
  }),
);

import { linkOrcid } from '../../src/resources/linkOrcid';

const mockContext = {
  body: { refreshToken: 'Arbitrary token', redirectUri: 'https://arbitrary.uri' },
  database: {} as any,

  headers: {},
  method: 'POST' as 'POST',
  params: [ '/arbitrary_user_id/orcid/verify/arbitrary_code', 'arbitrary_user_id', 'arbitrary_code' ],
  path: '/arbitrary_user_id/orcid/verify/arbitrary_code',
  query: null,
};

beforeEach(() => {
  process.env.orcid_base_path = 'https://arbitrary.orcid.basepath';
  process.env.orcid_client_id = 'arbitrary_id';
  process.env.orcid_client_secret = 'arbitrary_secret';
});

it('should return a new JWT when passed the correct values', () => {
  return expect(linkOrcid(mockContext)).resolves.toHaveProperty('jwt');
});

it('should return the account data when passed the correct values', async () => {
  const response = await linkOrcid(mockContext);

  expect(response.account).toBeDefined();
  expect(response.account.identifier).toBeDefined();
  expect(response.account.orcid).toBeDefined();
});

it('should error when no user ID and verification code were specified', () => {
  return expect(linkOrcid({
    ...mockContext,
    params: [],
    path: '/',
  })).rejects
    .toEqual(new Error('Please specify both a session ID and an ORCID verification code.'));
});

it('should error when the JWT could not be verified', () => {
  const mockVerifyJwt = require.requireMock('../../../../lib/lambda/verifyJwt').verifyJwt;
  mockVerifyJwt.mockReturnValueOnce(new Error('Verification error'));

  return expect(linkOrcid(mockContext)).rejects
    .toEqual(new Error('Something went wrong linking your session to your ORCID, please try again.'));
});

it('should error when the ORCID API returns an error', () => {
  const mockedFetch = require.requireMock('node-fetch').default.mockReturnValueOnce(
    Promise.resolve({ json: () => Promise.resolve({ error: 'Arbitrary error' }) }),
  );

  return expect(linkOrcid(mockContext)).rejects
    .toEqual(new Error('Your ORCID could not be confirmed, please try again.'));
});

// tslint:disable-next-line:max-line-length
it('should error when the ORCID is already linked to an account other than the one associated with the current session', () => {
  const mockGetAccountForOrcid = require.requireMock('../../src/dao').getAccountForOrcid;
  mockGetAccountForOrcid.mockReturnValueOnce(Promise.resolve({ id: 'Some ID' }));

  const mockVerifyJwt = require.requireMock('../../../../lib/lambda/verifyJwt').verifyJwt;
  mockVerifyJwt.mockReturnValueOnce({ identifier: 'Arbitrary ID', account: { identifier: 'Some other ID' } });

  return expect(linkOrcid(mockContext))
    .rejects.toEqual(new Error('This ORCID is already associated with another account.'));
});

it('should re-use an account already linked to the given ORCID', (done) => {
  const mockedDao = require.requireMock('../../src/dao');
  mockedDao.getAccountForOrcid.mockReturnValueOnce(Promise.resolve({ identifier: 'Some ID' }));

  linkOrcid(mockContext);

  setImmediate(() => {
    expect(mockedDao.storeOrcidCredentials.mock.calls.length).toBe(1);
    expect(mockedDao.storeOrcidCredentials.mock.calls[0][5]).toBe('Some ID');

    done();
  });
});

it('should associate the ORCID with the current account, if the user has one', (done) => {
  const mockedDao = require.requireMock('../../src/dao');
  mockedDao.getAccountForOrcid.mockReturnValueOnce(Promise.resolve(null));

  const mockVerifyJwt = require.requireMock('../../../../lib/lambda/verifyJwt').verifyJwt;
  mockVerifyJwt.mockReturnValueOnce({ identifier: 'Arbitrary ID', account: { identifier: 'Some ID' } });

  linkOrcid(mockContext);

  setImmediate(() => {
    expect(mockedDao.storeOrcidCredentials.mock.calls.length).toBe(1);
    expect(mockedDao.storeOrcidCredentials.mock.calls[0][5]).toBe('Some ID');

    done();
  });
});

it('should generate a new account ID if none is known yet', (done) => {
  const mockGetAccountForOrcid = require.requireMock('../../src/dao').getAccountForOrcid;
  mockGetAccountForOrcid.mockReturnValueOnce(Promise.resolve(null));

  const mockVerifyJwt = require.requireMock('../../../../lib/lambda/verifyJwt').verifyJwt;
  mockVerifyJwt.mockReturnValueOnce({ identifier: 'Arbitrary ID' });

  const mockedStoreOrcidCredentials = require.requireMock('../../src/dao').storeOrcidCredentials;

  linkOrcid(mockContext);

  setImmediate(() => {
    expect(mockedStoreOrcidCredentials.mock.calls.length).toBe(1);
    expect(mockedStoreOrcidCredentials.mock.calls[0][5]).toBeDefined();

    done();
  });
});

it('should error when the ORCID credentials could not be stored', () => {
  const mockStoreOrcidCredentials = require.requireMock('../../src/dao').storeOrcidCredentials;
  mockStoreOrcidCredentials.mockReturnValueOnce(Promise.reject(new Error('Arbitrary error')));

  return expect(linkOrcid(mockContext)).rejects
    .toEqual(new Error('Something went wrong verifying your ORCID, please try again.'));
});

it('should error when the JWT could not be created', () => {
  const mockSignJwt = require.requireMock('../../../../lib/lambda/signJwt').signJwt;
  mockSignJwt.mockReturnValueOnce(new Error('Arbitrary signing error'));

  return expect(linkOrcid(mockContext)).rejects
    .toEqual(new Error('We are currently experiencing issues, please try again later.'));
});
