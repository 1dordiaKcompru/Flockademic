import { GetIsPeriodicalSlugAvailableResponse } from '../../../../lib/interfaces/endpoints/periodical';
import { Request } from '../../../../lib/lambda/faas';
import { DbContext } from '../../../../lib/lambda/middleware/withDatabase';
import { isPeriodicalSlugAvailable as commandHandler } from '../services/isPeriodicalSlugAvailable';

export async function isPeriodicalSlugAvailable(
  context: Request<undefined> & DbContext,
): Promise<GetIsPeriodicalSlugAvailableResponse> {
  if (!context.params || context.params.length < 2) {
    throw(new Error('Could not find a journal without a journal ID.'));
  }

  try {
    const isAvailable = await commandHandler(context.database, context.params[1]);

    return isAvailable
      ? { error: { description: 'No existing periodical found with that name.' } }
      : { result: { identifier: context.params[1] } };
  } catch (e) {
      throw new Error('There was a problem checking for the journal\'s existence.');
  }
}
